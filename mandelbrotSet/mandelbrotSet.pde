PImage canvas;
void settings () {
  size (600,600);
  canvas = createImage(600,600,RGB);
}
static final long MAX = 100000L;
static final int T = 2;
double STEP = (double)1/100;
int M;
double MULTIPLIER = (double)1/2000;
double trX = 0;
double trY = 0;

void setup() {
  M = 1; //MAX number of iterations
}
void keyPressed() {
  if (key == CODED) {
    if (keyCode == LEFT) {
      trX -= STEP;
    }
    else if (keyCode == RIGHT)
      trX += STEP;
    else if (keyCode == UP)
      trY -= STEP;
    else if (keyCode == DOWN)
      trY += STEP;
  }
  if (key == '+') {
    MULTIPLIER *= 0.5;
    trX += 8*STEP;
    trY += 8*STEP;
    STEP *= 0.5;
  }
  else if (key == '-') {
    MULTIPLIER /= 0.5;
    STEP /= 0.5;
  }
  double x0 = MULTIPLIER*0+trX;
  double xf = MULTIPLIER*599+trX;
  double y0 = MULTIPLIER*0+trY;
  double yf = MULTIPLIER*599+trY;
  println("x0: "+ x0);
  println("xf: "+ xf);
  println("y0: "+ y0);
  println("yf: "+ yf);
}

void draw() {
  if (M < MAX) {
    M += 1;
  }
  canvas.loadPixels();
  
  for (int x = 0; x < 600; x++) {
    for (int y = 0; y < 600; y++) {
      color c;
      Complex iter = new Complex (MULTIPLIER*x+trX, MULTIPLIER*y+trY);
      while (iter.n < M && iter.module() <= T) {
        iter.next();
      }
      //if (iter.module() <= 2){
      //println("x = "+x+" y = "+y);
      //println ("n = " + iter.n);
      //println ("abs z = "+iter.module());
      //}
      if (iter.n < M) {
        //diverge
        c = color(round(255*(float)iter.n/M),0,100);
      }
      else {
        //converge
        c = color(0,200,0);
      }
      canvas.pixels[index(x, y, canvas.width)] = c;
      
    }
  }
  canvas.updatePixels();
  image(canvas,0,0);
}

int index (int row, int col, int n) {
  return (row + col*n);
}
class Complex {
  double a;
  double b;
  double ca;
  double cb;
  int n;
  public Complex(double ca, double cb) {
    this.a = 0;
    this.b = 0;
    this.ca = ca;
    this.cb = cb;
    n = 0;
  }
  public double module() {
    return sqrt((float)a*(float)a+(float)b*(float)b);
  }
  public void next() {
    //f(z) = z^2 + c
    //(a+bi)^2 = a^2 + 2abi -b^2
    double novoA = a*a-b*b + ca;
    double novoB = 2*a*b + cb;
    a = novoA;
    b = novoB;
    ++n;
  }
  public void next3() {
    //f(z) = z^3 + c
    //(a+bi)^3 = a^3 -bi^3 +3a^2bi -3ab^2
    //(a+bi)^2 = a^3-3ab^2 +i(3a^2b - b^3)
    double novoA = a*(a*a-3*b*b)+ca;
    double novoB = b*(3*a*a-b*b)+cb;
    a = novoA;
    b = novoB;
    ++n;
  }
  public void next4() {
    //f(z) = z^3+z^2 + c
    //(a+bi)^3 = a^3 -bi^3 +3a^2bi -3ab^2
    //(a+bi)^2 = a^3-3ab^2 +i(3a^2b - b^3)
    double novoA = a*(a*a-3*b*b+a)-b*b+ca;
    double novoB = b*(3*a*a-b*b+2*a)+cb;
    a = novoA;
    b = novoB;
    ++n;
  }
}

/*class ComplexJ extends Complex {
    public ComplexJ(double za, double zb) {
    this.a = za*za-zb*zb;
    this.b = 2*za*zb;
    this.ca = 0;
    this.cb = 0;
    this.n = 0;
  }
  public void next() {
    //f(c) = z^2+c;
    // f = a+bi
    //(a+bi)^ = a^2 +2abi - b^2
    double novoA = a+a;
    double novoB = b+b;
    a = novoA;
    b = novoB;
  }
}*/
